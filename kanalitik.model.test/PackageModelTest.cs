﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class PackageModelTest
    {
        PackageModel model = new PackageModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new Package
            {
                blog = new string[] { },
                created_by = model.IdGuid,
                is_show = true,
                modified_by = model.IdGuid,
                module = new List<ModulePackage>() { },
                news_portal = new string[] { },
                price = 12500000,
                social_media = new string[] { }
            };

            var actual = model.createOne(data).GetAwaiter().GetResult();
            //Assert.AreEqual("Creoactive", actual.name);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new Package
            {
                blog = new string[] { "description1", "description2" },
                created_by = model.IdGuid,
                is_show = true,
                modified_by = model.IdGuid,
                module = new List<ModulePackage>() { },
                news_portal = new string[] { "description1", "description2" },
                price = 5250000,
                social_media = new string[] { "description1", "description2" }

            }).GetAwaiter().GetResult();

            //Assert.AreEqual("Creoactive Update", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual.id.ToString());
        }

        //[TestMethod]
        //public void isEmailExistTest()
        //{
        //    var actual = model.isExist("mail@mail.com").GetAwaiter().GetResult();
        //    Assert.AreEqual("mail@mail.com", actual.email);
        //}

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "tru3.d3v@gmail.com")), new BsonDocument(new BsonElement("name", -1)), 0, 1).GetAwaiter().GetResult();

            Assert.AreEqual(1, actual.Length);

        }

    }
}
