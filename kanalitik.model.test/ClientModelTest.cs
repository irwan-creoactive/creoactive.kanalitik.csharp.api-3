﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using kanalitik.constanta;

namespace kanalitik.model.test
{

    [TestClass]
    public class ClientModelTest
    {
        ClientModel model = new ClientModel();

        [TestMethod]
        public void createOneTest()
        {
            var data = new Client
            {
                members = new List<MemberClient>()
                {
                    //new MemberClient(){id="112233",role_user=RoleUserEnum.Account_Owner_Demo.ToString(),status=true}
                },
                name = "Creoactive",
                owned_user = "5549c69bd2b8de6838e84883",
                packages = new List<PackageClient>()
                {
                    //new PackageClient(){active_since=20150506000000}
                },
                status = true,
                created_by = "test",
                modified_by = "test"
            };

            var actual = model.createOne(data).GetAwaiter().GetResult();
            Assert.AreEqual("Creoactive", actual.name);
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new Client
            {
                id = new ObjectId("5549d41ed2b8de0f20376c41"),
                members = new List<MemberClient>()
                {
                    new MemberClient(){id="112233",role_user=RoleUserEnum.Account_Owner_Demo.ToString(),status=true}
                },
                name = "Creoactive Update",
                owned_user = "5549c69bd2b8de6838e84883",
                packages = new List<PackageClient>()
                {
                    new PackageClient(){active_since=20150506000000}
                },
                status = true,
                created_by = "test",
                modified_by = "test"

            }).GetAwaiter().GetResult();

            Assert.AreEqual("Creoactive Update", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual.id.ToString());
        }

        //[TestMethod]
        //public void isEmailExistTest()
        //{
        //    var actual = model.isExist("mail@mail.com").GetAwaiter().GetResult();
        //    Assert.AreEqual("mail@mail.com", actual.email);
        //}

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "tru3.d3v@gmail.com")), new BsonDocument(new BsonElement("name", -1)), 0, 1).GetAwaiter().GetResult();

            Assert.AreEqual(1, actual.Length);

        }

    }
}
