﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class RoleUserModelTest
    {
        RoleUserModel model = new RoleUserModel();

        [TestMethod]
        public void createSampleData()
        {
            List<RoleUser> role_users = new List<RoleUser>();
            List<Privillage> privillages = new List<Privillage>();

            privillages.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "add/removefacebookaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "add/removetwitteraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;facebook", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;twitter", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Feature", code_object = "unwantedmarkingfeatures", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Module", code_object = "myaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Module", code_object = "otheraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "0", name = "Account Owner - Demo", description = "Client Demo - Account Owner", privillages = privillages, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_1 = new List<Privillage>();

            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "changeaccountname", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "changebusinesstype", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "viewpackageplaninfo&status", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "stopcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "renewcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "view/changemembername", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "changeloginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "changeloginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "changemobilephonecontact", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "askfortech.support(viaticketing)", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "startdiscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "deletediscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "add/removefacebookaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "add/removetwitteraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "add/removekeyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-socialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofblogs", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofkeywords", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;facebook", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;twitter", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "unwantedmarkingfeatures", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "postsrevision", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "myaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "otheraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "keyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_1.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "1", name = "Account Owner - Free", description = "Client Free - Account Owner", privillages = privillages_1, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_2 = new List<Privillage>();

            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "changeaccountname", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "changebusinesstype", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewpackageplaninfo&status", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "stopcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "renewcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "view/changemembername", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "changeloginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "changeloginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "changemobilephonecontact", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "deleteteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/inviteteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "assignrolesforteammember", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "askfortech.support(viaticketing)", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "startdiscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "deletediscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "chooseobjectiveoptions", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "setobjectiveamount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "changeobjectiveamount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "deleteobjective", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewobjectiveprogress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removefacebookaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removetwitteraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removegoogle+account", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removeyoutubeaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removeinstagramaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removewordpressblog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removegeneralportal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removeclassifiedportal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removekeyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-socialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-portal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofportals", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofblogs", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofkeywords", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;facebook", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;twitter", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;youtube", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "unwantedmarkingfeatures", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "postsapproval", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "postsrevision", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "view&receivereport;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Feature", code_object = "setreportdeliverytime", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "myaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "otheraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "keyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "teammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_2.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "2", name = "Account Owner", description = "Client - Account Owner", privillages = privillages_2, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_3 = new List<Privillage>();

            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "changeaccountname", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "changebusinesstype", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "view/changemembername", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "changeloginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "changeloginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "changemobilephonecontact", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "deleteteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/inviteteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "assignrolesforteammember", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "askfortech.support(viaticketing)", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "startdiscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "setobjectiveamount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "changeobjectiveamount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewobjectiveprogress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removefacebookaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removetwitteraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removegoogle+account", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removeyoutubeaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removeinstagramaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removewordpressblog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removegeneralportal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removeclassifiedportal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removekeyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-socialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-portal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofportals", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofblogs", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofkeywords", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "unwantedmarkingfeatures", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "postsapproval", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "postsrevision", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Feature", code_object = "view&receivereport;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "myaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "otheraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "keyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "teammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_3.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "3", name = "Account Administrator", description = "Client - Account Administrator", privillages = privillages_3, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_4 = new List<Privillage>();

            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewpackageplaninfo&status", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "startdiscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewobjectiveprogress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofportals", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofblogs", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofkeywords", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "providetechnicalsupport", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "prepare&sendreport", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Feature", code_object = "senddue-paymentinfo", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Module", code_object = "teammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_4.Add(new Privillage { type_object = "Module", code_object = "systemadmin", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "4", name = "Client Service", description = "Service Provider - Client Service", privillages = privillages_4, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_5 = new List<Privillage>();

            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "view/changemembername", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "changeloginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "changeloginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "changemobilephonecontact", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "askfortech.support(viaticketing)", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "startdiscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewobjectiveprogress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofportals", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofblogs", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofkeywords", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;facebook", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;twitter", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;youtube", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "unwantedmarkingfeatures", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "postsrevision", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Feature", code_object = "view&receivereport;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Module", code_object = "teammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_5.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "5", name = "Admin Head", description = "Client - Admin Head", privillages = privillages_5, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_6 = new List<Privillage>();

            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "view/changemembername", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "changeloginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "changeloginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "changemobilephonecontact", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "askfortech.support(viaticketing)", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "startdiscussion/chatboard", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewobjectiveprogress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofsocialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofportals", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofblogs", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewpostsofkeywords", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;facebook", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;twitter", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;youtube", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "postcreation&engagement;blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "unwantedmarkingfeatures", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "postsrevision", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Module", code_object = "teammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_6.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "6", name = "Basic Admin", description = "Client - Basic Admin", privillages = privillages_6, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_7 = new List<Privillage>();

            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "changeaccountname", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "changebusinesstype", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "viewpackageplaninfo&status", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "stopcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "renewcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "view/changemembername", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "changeloginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "changeloginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "changemobilephonecontact", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "viewteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "deleteteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/inviteteammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "assignrolesforteammember", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "chooseobjectiveoptions", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "setobjectiveamount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "changeobjectiveamount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "deleteobjective", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "viewobjectiveprogress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removefacebookaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removetwitteraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removegoogle+account", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removeyoutubeaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removeinstagramaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removewordpressblog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removegeneralportal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removeclassifiedportal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removekeyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-socialmedia", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-portal", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "add/removesource-blog", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "past-scheduledpostsaccess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "viewanalysisstreams;basedonpackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "activate/deactivateclient'skanalitikaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "assignclientservicewithemailaddress", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "manageactivationprocess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "manageforgetpasswordprocess", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Feature", code_object = "providetechnicalsupport", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "systemadmin", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "configuration", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "myaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "otheraccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "keyword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "report", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "teammembers", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_7.Add(new Privillage { type_object = "Module", code_object = "streams", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "7", name = "System Administrator", description = "Service Provider - System Administrator", privillages = privillages_7, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            List<Privillage> privillages_8 = new List<Privillage>();

            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "loginemail", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "loginpassword", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "viewpackageplaninfo&status", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "renewcurrentpackageplan", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "prepare&sendbillingstatement/invoice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "activate/deactivateclient'skanalitikaccount", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "receivepayment", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "upgrade/downgradepackage", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Feature", code_object = "senddue-paymentinfo", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Module", code_object = "homedeckoffice", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });
            privillages_8.Add(new Privillage { type_object = "Module", code_object = "systemadmin", can_change = "TRUE", can_view = "TRUE", can_remove = "TRUE", can_post = "FALSE", can_approve = "FALSE" });

            role_users.Add(new RoleUser { role_id = "8", name = "Finance Staff", description = "Service Provider - Finance Staff", privillages = privillages_8, created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });

            var data = role_users;
            model.createMany(data).GetAwaiter().GetResult();
        }

        [TestMethod]
        public Object createOneTest()
        {
            var data = new RoleUser
            {
                name = "System Administrator",
                description = "Untuk mengatur isian data yang terkait dengan sistem.",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            //var expectedResultName = "System Administrator";
            //Assert.AreEqual(expectedResultName, actual.name);

            return actual.id;
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new RoleUser
            {
                id = new ObjectId("55471eab9c876e1720763ee8"),
                name = "System Administratorsss",
                description = "Untuk mengatur isian data yang terkait dengan sistem.",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("System Administratorsss", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
