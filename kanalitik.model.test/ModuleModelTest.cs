﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class ModuleModelTest
    {
        ModuleModel model = new ModuleModel();

        [TestMethod]
        public void createSampleData()
        {
            List<Module> modules = new List<Module>();

            // untuk isian data master module, diambil dari https://docs.google.com/a/creoactive.id/spreadsheets/d/1f4lamtUE0CjvEUVbdch1-xxa1r0zAT42Vgmboruwrh0/edit#gid=1674548759 sheet module

            modules.Add(new Module { code = "homedeckoffice", name = "Home Deck Office", seq = "1", description = "", is_client = "FALSE", client_id = "", parent_module_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "systemadmin", name = "System Admin", seq = "2", description = "", is_client = "FALSE", client_id = "", parent_module_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "configuration", name = "Configuration", seq = "3", description = "", is_client = "FALSE", client_id = "", parent_module_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "myaccount", name = "My Account", seq = "1", description = "", is_client = "FALSE", client_id = "", parent_module_code = "configuration", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "otheraccount", name = "Other Account", seq = "2", description = "", is_client = "FALSE", client_id = "", parent_module_code = "configuration", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "keyword", name = "Keyword", seq = "3", description = "", is_client = "FALSE", client_id = "", parent_module_code = "configuration", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "report", name = "Report", seq = "4", description = "", is_client = "FALSE", client_id = "", parent_module_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "teammembers", name = "Team Members", seq = "5", description = "", is_client = "FALSE", client_id = "", parent_module_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "streams", name = "Streams", seq = "6", description = "", is_client = "FALSE", client_id = "", parent_module_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            modules.Add(new Module { code = "configuration", name = "Configuration", seq = "1", description = "", is_client = "FALSE", client_id = "", parent_module_code = "streams", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });

            var data = modules;
            model.createMany(data).GetAwaiter().GetResult();
        }

        [TestMethod]
        public Object createOneTest()
        {
            var data = new Module
            {
                name = "System Administrator",
                description = "Untuk mengatur isian data yang terkait dengan sistem.",
                is_client = "true",
                client_id = "",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            //var expectedResultName = "System Administrator";
            //Assert.AreEqual(expectedResultName, actual.name);

            return actual.id;
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new Module
            {
                id = new ObjectId("55471eab9c876e1720763ee8"),
                name = "System Administrator",
                description = "Untuk mengatur isian data yang terkait dengan sistem.",
                is_client = "false",
                client_id = "",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("false", actual.is_client);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
