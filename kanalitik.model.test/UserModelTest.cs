﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;

namespace kanalitik.model.test
{

    [TestClass]
    public class UserModelTest
    {
        UserModel model = new UserModel();

        [TestMethod]
        public void createOneTest()
        {
            string passwordGenerate = SecurityUtility.encrypt(SecurityUtility.generatePassword());
            var data = new User
            {
                name = "Creoactive",
                email = "tru3.d3v@gmail.com",
                business_type = "Personal",
                role_user = "1",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow,
                password = passwordGenerate
            };
           var actual =  model.createOne(data).GetAwaiter().GetResult();
            var expectedResultEmail = "tru3.d3v@gmail.com";
            Assert.AreEqual(expectedResultEmail, actual.email);
        }

        [TestMethod]
        public void updateOneTest()
        {
            
           var actual = model.updateOne(new User
            {
                id = new ObjectId("55408db70234c92a9477c008"),
                name = "halloxx",
                email = "mail@mail.com" ,
                password="admin",
                business_type="Company",
                role_user="1",
                modified_by=model.IdGuid,
                modified_date = DateTime.UtcNow,
                client = new string[] { "A","xxB"}
            }).GetAwaiter().GetResult();

           Assert.AreEqual("mail@mail.com", actual.email);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual.id.ToString());
        }

        [TestMethod]
        public void isEmailExistTest()
        {
            var actual = model.isEmailExist("mail@mail.com").GetAwaiter().GetResult();
            Assert.AreEqual("mail@mail.com", actual.email);
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55408db70234c92a9477c008").GetAwaiter().GetResult();
            Assert.AreEqual("55408db70234c92a9477c008", actual);
        }

        [TestMethod]
        public void listTest()
        {
            var actual = model.list(new BsonDocument(new BsonElement("email", "tru3.d3v@gmail.com")), new BsonDocument(new BsonElement("name", -1)), 0, 1).GetAwaiter().GetResult();

            Assert.AreEqual(1, actual.Length);

        }

    }
}
