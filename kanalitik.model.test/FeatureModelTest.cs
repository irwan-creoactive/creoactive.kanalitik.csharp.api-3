﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanalitik.models;
using kanalitik.data.access.documents;
using kanalitik.utility;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace kanalitik.model.test
{

    [TestClass]
    public class FeatureModelTest
    {
        FeatureModel model = new FeatureModel();

        [TestMethod]
        public void createSampleData()
        {
            List<Feature> features = new List<Feature>();

            // untuk isian data master module, diambil dari https://docs.google.com/a/creoactive.id/spreadsheets/d/1f4lamtUE0CjvEUVbdch1-xxa1r0zAT42Vgmboruwrh0/edit#gid=1674548759 sheet feature

            features.Add(new Feature { code = "dedicatedlogin", name = "Dedicated Login", seq = "1", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "loginemail", name = "Log In Email", seq = "1", description = "", parent_feature_code = "dedicatedlogin", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "loginpassword", name = "Log In Password", seq = "2", description = "", parent_feature_code = "dedicatedlogin", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "accountsettings", name = "Account Settings", seq = "2", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "projectaccountsettings", name = "Project Account Settings", seq = "1", description = "", parent_feature_code = "accountsettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "changeaccountname", name = "Change Account Name", seq = "1", description = "", parent_feature_code = "projectaccountsettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "changebusinesstype", name = "Change Business Type", seq = "2", description = "", parent_feature_code = "projectaccountsettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "billingmanagement", name = "Billing Management", seq = "2", description = "", parent_feature_code = "accountsettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewpackageplaninfo&status", name = "View Package Plan Info & Status", seq = "1", description = "", parent_feature_code = "billingmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "stopcurrentpackageplan", name = "Stop Current Package Plan", seq = "2", description = "", parent_feature_code = "billingmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "renewcurrentpackageplan", name = "Renew Current Package Plan", seq = "3", description = "", parent_feature_code = "billingmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "upgrade/downgradepackage", name = "Upgrade/Downgrade Package", seq = "4", description = "", parent_feature_code = "billingmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "membersettings", name = "Member Settings", seq = "3", description = "", parent_feature_code = "accountsettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "view/changemembername", name = "View/Change Member Name", seq = "1", description = "", parent_feature_code = "membersettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "changeloginemail", name = "Change Log In Email", seq = "2", description = "", parent_feature_code = "membersettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "changeloginpassword", name = "Change Log In Password", seq = "3", description = "", parent_feature_code = "membersettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "changemobilephonecontact", name = "Change Mobile Phone Contact", seq = "4", description = "", parent_feature_code = "membersettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "workflowmanagement", name = "Workflow Management", seq = "4", description = "", parent_feature_code = "accountsettings", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewteammembers", name = "View Team Members", seq = "1", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "deleteteammembers", name = "Delete Team Members", seq = "2", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/inviteteammembers", name = "Add/Invite Team Members", seq = "3", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "assignrolesforteammember", name = "Assign Roles for Team Member", seq = "4", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "askfortech.support(viaticketing)", name = "Ask for Tech. Support (via Ticketing)", seq = "5", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "startdiscussion/chatboard", name = "Start Discussion/Chat Board", seq = "6", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "deletediscussion/chatboard", name = "Delete Discussion/Chat Board", seq = "7", description = "", parent_feature_code = "workflowmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "project'skpi/objective", name = "Project's KPI/Objective", seq = "3", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "chooseobjectiveoptions", name = "Choose Objective Options", seq = "1", description = "", parent_feature_code = "project'skpi/objective", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "setobjectiveamount", name = "Set Objective Amount", seq = "2", description = "", parent_feature_code = "project'skpi/objective", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "changeobjectiveamount", name = "Change Objective Amount", seq = "3", description = "", parent_feature_code = "project'skpi/objective", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "deleteobjective", name = "Delete Objective", seq = "4", description = "", parent_feature_code = "project'skpi/objective", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewobjectiveprogress", name = "View Objective Progress", seq = "5", description = "", parent_feature_code = "project'skpi/objective", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removechannels&keywords", name = "Add/Remove Channels & Keywords", seq = "4", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "socialchannels", name = "Social Channels", seq = "1", description = "", parent_feature_code = "add/removechannels&keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removefacebookaccount", name = "Add/Remove Facebook Account", seq = "1", description = "", parent_feature_code = "socialchannels", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removetwitteraccount", name = "Add/Remove Twitter Account", seq = "2", description = "", parent_feature_code = "socialchannels", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removegoogle+account", name = "Add/Remove Google+ Account", seq = "3", description = "", parent_feature_code = "socialchannels", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removeyoutubeaccount", name = "Add/Remove YouTube Account", seq = "4", description = "", parent_feature_code = "socialchannels", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removeinstagramaccount", name = "Add/Remove Instagram Account", seq = "5", description = "", parent_feature_code = "socialchannels", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removewordpressblog", name = "Add/Remove WordPress Blog", seq = "6", description = "", parent_feature_code = "socialchannels", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "newsportal", name = "News Portal", seq = "2", description = "", parent_feature_code = "add/removechannels&keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removegeneralportal", name = "Add/Remove General Portal", seq = "1", description = "", parent_feature_code = "newsportal", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removeclassifiedportal", name = "Add/Remove Classified Portal", seq = "2", description = "", parent_feature_code = "newsportal", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "keywords", name = "Keywords", seq = "3", description = "", parent_feature_code = "add/removechannels&keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removekeyword", name = "Add/Remove Keyword", seq = "1", description = "", parent_feature_code = "keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removesource-socialmedia", name = "Add/Remove Source - Social Media", seq = "2", description = "", parent_feature_code = "keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removesource-portal", name = "Add/Remove Source - Portal", seq = "3", description = "", parent_feature_code = "keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "add/removesource-blog", name = "Add/Remove Source - Blog", seq = "4", description = "", parent_feature_code = "keywords", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "streamsmanagement", name = "Streams Management", seq = "5", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postsviewers;my&otheraccounts", name = "Posts Viewers; My & Other Accounts", seq = "1", description = "", parent_feature_code = "streamsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewpostsofsocialmedia", name = "View Posts of Social Media", seq = "1", description = "", parent_feature_code = "postsviewers;my&otheraccounts", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewpostsofportals", name = "View Posts of Portals", seq = "2", description = "", parent_feature_code = "postsviewers;my&otheraccounts", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewpostsofblogs", name = "View Posts of Blogs", seq = "3", description = "", parent_feature_code = "postsviewers;my&otheraccounts", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewpostsofkeywords", name = "View Posts of Keywords", seq = "4", description = "", parent_feature_code = "postsviewers;my&otheraccounts", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postsmanagement", name = "Posts Management", seq = "2", description = "", parent_feature_code = "streamsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postcreation&engagement;facebook", name = "Post Creation & Engagement; Facebook", seq = "1", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postcreation&engagement;twitter", name = "Post Creation & Engagement; Twitter", seq = "2", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postcreation&engagement;youtube", name = "Post Creation & Engagement; YouTube", seq = "3", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postcreation&engagement;blog", name = "Post Creation & Engagement; Blog", seq = "4", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "unwantedmarkingfeatures", name = "Unwanted Marking Features", seq = "5", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postsapproval", name = "Posts Approval", seq = "6", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "past-scheduledpostsaccess", name = "Past-scheduled Posts Access", seq = "7", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "postsrevision", name = "Posts Revision", seq = "8", description = "", parent_feature_code = "postsmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewanalysisresults", name = "View Analysis Results", seq = "6", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "viewanalysisstreams;basedonpackage", name = "View Analysis Streams; based on Package", seq = "1", description = "", parent_feature_code = "viewanalysisresults", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "reporting", name = "Reporting", seq = "7", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "view&receivereport;basedonpackage", name = "View & Receive Report; Based on Package", seq = "1", description = "", parent_feature_code = "reporting", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "setreportdeliverytime", name = "Set Report Delivery Time", seq = "2", description = "", parent_feature_code = "reporting", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "accountmanagement", name = "Account Management", seq = "8", description = "", parent_feature_code = "", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "client'skanalitikaccount", name = "Client's Kanalitik Account", seq = "1", description = "", parent_feature_code = "accountmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "activate/deactivateclient'skanalitikaccount", name = "Activate/Deactivate Client's Kanalitik Account", seq = "1", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "assignclientservicewithemailaddress", name = "Assign Client Service with Email Address", seq = "2", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "manageactivationprocess", name = "Manage Activation Process", seq = "3", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "manageforgetpasswordprocess", name = "Manage Forget Password Process", seq = "4", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "upgrade/downgradepackage", name = "Upgrade/Downgrade Package", seq = "5", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "providetechnicalsupport", name = "Provide Technical Support", seq = "6", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "prepare&sendreport", name = "Prepare & Send Report", seq = "7", description = "", parent_feature_code = "client'skanalitikaccount", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "billingandfinance", name = "Billing and Finance", seq = "2", description = "", parent_feature_code = "accountmanagement", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "prepare&sendbillingstatement/invoice", name = "Prepare & Send Billing Statement/Invoice", seq = "1", description = "", parent_feature_code = "billingandfinance", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "activate/deactivateclient'skanalitikaccount", name = "Activate/Deactivate Client's Kanalitik Account", seq = "2", description = "", parent_feature_code = "billingandfinance", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "receivepayment", name = "Receive Payment", seq = "3", description = "", parent_feature_code = "billingandfinance", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "upgrade/downgradepackage", name = "Upgrade/Downgrade Package", seq = "4", description = "", parent_feature_code = "billingandfinance", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });
            features.Add(new Feature { code = "senddue-paymentinfo", name = "Send Due-Payment Info", seq = "5", description = "", parent_feature_code = "billingandfinance", created_by = model.IdGuid, modified_by = model.IdGuid, created_date = DateTime.UtcNow, modified_date = DateTime.UtcNow });

            var data = features;
            model.createMany(data).GetAwaiter().GetResult();
        }

        [TestMethod]
        public Object createOneTest()
        {
            var data = new Feature
            {
                name = "System Administrator",
                description = "Untuk mengatur isian data yang terkait dengan sistem.",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            };
            var actual = model.createOne(data).GetAwaiter().GetResult();
            //var expectedResultName = "System Administrator";
            //Assert.AreEqual(expectedResultName, actual.name);

            return actual.id;
        }

        [TestMethod]
        public void updateOneTest()
        {

            var actual = model.updateOne(new Feature
            {
                id = new ObjectId("55471eab9c876e1720763ee8"),
                name = "System Administrator",
                description = "Untuk mengatur isian data yang terkait dengan sistem.",
                created_by = model.IdGuid, // id user login
                modified_by = model.IdGuid, // id user login
                created_date = DateTime.UtcNow,
                modified_date = DateTime.UtcNow
            }).GetAwaiter().GetResult();

            Assert.AreEqual("System Administrator", actual.name);
        }

        [TestMethod]
        public void createMany()
        {
            // TODO
        }

        [TestMethod]
        public void updateManyTest()
        {
            // TODO      
        }

        [TestMethod]
        public void getByIdTest()
        {
            var actual = model.getById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual.id.ToString());
        }

        [TestMethod]
        public void deleteByIdTest()
        {
            var actual = model.deleteById("55471eab9c876e1720763ee8").GetAwaiter().GetResult();
            Assert.AreEqual("55471eab9c876e1720763ee8", actual);
        }

    }
}
