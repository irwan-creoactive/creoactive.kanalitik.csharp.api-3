﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class BlogModel : BaseModel<Blog>, IBaseModel<Blog>
    {

        public BlogModel() : base("BlogModel") { }

        public async Task<Blog> createOne(Blog data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "BlogInsertRuleSet");
            Task<Blog> result = null;
            if (validation.IsValid)
            {
                await this.db.Blog.InsertOneAsync(data);
                result = Task<Blog>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Blog> updateOne(Blog data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "BlogUpdateRuleSet");

            Task<Blog> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Blog>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Blog.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Blog>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Blog> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Blog.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Blog> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Blog> getById(string id)
        {
            var builderFilter = Builders<Blog>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Blog> result = null;
            using (var cursor = await this.db.Blog.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Blog>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Blog>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Blog.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Blog[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Blog[]> result = null;
            List<Blog> res = new List<Blog>();
            using (var cursor = await this.db.Blog.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Blog[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
