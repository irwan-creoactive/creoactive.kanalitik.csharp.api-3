﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class DiscussionBoardModel : BaseModel<DiscussionBoard>, IBaseModel<DiscussionBoard>
    {

        public DiscussionBoardModel() : base("DiscussionBoardModel") { }

        public async Task<DiscussionBoard> createOne(DiscussionBoard data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "DiscussionBoardInsertRuleSet");
            Task<DiscussionBoard> result = null;
            if (validation.IsValid)
            {
                await this.db.DiscussionBoard.InsertOneAsync(data);
                result = Task<DiscussionBoard>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<DiscussionBoard> updateOne(DiscussionBoard data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "DiscussionBoardUpdateRuleSet");

            Task<DiscussionBoard> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<DiscussionBoard>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.DiscussionBoard.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<DiscussionBoard>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<DiscussionBoard> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "DiscussionBoardInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.DiscussionBoard.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<DiscussionBoard> data)
        {
            throw new NotImplementedException();
        }

        public async Task<DiscussionBoard> getById(string id)
        {
            var builderFilter = Builders<DiscussionBoard>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<DiscussionBoard> result = null;
            using (var cursor = await this.db.DiscussionBoard.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<DiscussionBoard>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<DiscussionBoard>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.DiscussionBoard.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<DiscussionBoard[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<DiscussionBoard[]> result = null;
            List<DiscussionBoard> res = new List<DiscussionBoard>();
            using (var cursor = await this.db.DiscussionBoard.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<DiscussionBoard[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
