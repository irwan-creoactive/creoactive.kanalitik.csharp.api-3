﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class KeywordModel : BaseModel<Keyword>, IBaseModel<Keyword>
    {

        public KeywordModel() : base("KeywordModel") { }

        public async Task<Keyword> createOne(Keyword data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "KeywordCreateRuleSet");
            Task<Keyword> result = null;
            if (validation.IsValid)
            {
                await this.db.Keyword.InsertOneAsync(data);
                result = Task<Keyword>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Keyword> updateOne(Keyword data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "KeywordUpdateRuleSet");

            Task<Keyword> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Keyword>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Keyword.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Keyword>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Keyword> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "KeywordCreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Keyword.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Keyword> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Keyword> getById(string id)
        {
            var builderFilter = Builders<Keyword>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Keyword> result = null;
            using (var cursor = await this.db.Keyword.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Keyword>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Keyword>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Keyword.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Keyword[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Keyword[]> result = null;
            List<Keyword> res = new List<Keyword>();
            using (var cursor = await this.db.Keyword.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Keyword[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
