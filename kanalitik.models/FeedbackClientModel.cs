﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{
    public class FeedbackClientModel : BaseModel<FeedbackClient>, IBaseModel<FeedbackClient>
    {
        public FeedbackClientModel() : base("FeedbackClientModel") { }

        public async Task<FeedbackClient> createOne(FeedbackClient data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "FeedbackClientCreateRuleSet");
            Task<FeedbackClient> result = null;
            if (validation.IsValid)
            {
                await this.db.FeedbackClient.InsertOneAsync(data);
                result = Task<FeedbackClient>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<FeedbackClient> updateOne(FeedbackClient data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "FeedbackClientUpdateRuleSet");

            Task<FeedbackClient> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<FeedbackClient>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.FeedbackClient.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<FeedbackClient>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<FeedbackClient> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "FeedbackClientCreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.FeedbackClient.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<FeedbackClient> data)
        {
            throw new NotImplementedException();
        }

        public async Task<FeedbackClient> getById(string id)
        {
            var builderFilter = Builders<FeedbackClient>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<FeedbackClient> result = null;
            using (var cursor = await this.db.FeedbackClient.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<FeedbackClient>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<FeedbackClient>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.FeedbackClient.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<FeedbackClient[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<FeedbackClient[]> result = null;
            List<FeedbackClient> res = new List<FeedbackClient>();
            using (var cursor = await this.db.FeedbackClient.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<FeedbackClient[]>.Run(() =>
                {
                    return res.ToArray();
                });

            return await result;
        }
    }
}
