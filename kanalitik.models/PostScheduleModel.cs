﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class PostScheduleModel : BaseModel<PostSchedule>, IBaseModel<PostSchedule>
    {

        public PostScheduleModel() : base("PostScheduleModel") { }

        public async Task<PostSchedule> createOne(PostSchedule data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "PostScheduleInsertRuleSet");
            Task<PostSchedule> result = null;
            if (validation.IsValid)
            {
                await this.db.PostSchedule.InsertOneAsync(data);
                result = Task<PostSchedule>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<PostSchedule> updateOne(PostSchedule data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "PostScheduleUpdateRuleSet");

            Task<PostSchedule> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<PostSchedule>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.PostSchedule.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<PostSchedule>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<PostSchedule> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "PostScheduleInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.PostSchedule.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<PostSchedule> data)
        {
            throw new NotImplementedException();
        }

        public async Task<PostSchedule> getById(string id)
        {
            var builderFilter = Builders<PostSchedule>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<PostSchedule> result = null;
            using (var cursor = await this.db.PostSchedule.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<PostSchedule>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<PostSchedule>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.PostSchedule.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<PostSchedule[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<PostSchedule[]> result = null;
            List<PostSchedule> res = new List<PostSchedule>();
            using (var cursor = await this.db.PostSchedule.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<PostSchedule[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
