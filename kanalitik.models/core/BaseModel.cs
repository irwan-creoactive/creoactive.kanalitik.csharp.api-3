﻿using kanalitik.data.access;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{
   public class BaseModel<T>
    {
       ConfigurationValidatorFactory factory;
       public DBKanalitikContext db{set;get;}

       public string IdGuid { get { return Guid.NewGuid().ToString(); } }
       public string FileValidationBlock { set; get; }
        public BaseModel(string _fileValidationBlock)
        {
            db = new DBKanalitikContext();
            string cultureName = Thread.CurrentThread.CurrentCulture.Name;
            this.FileValidationBlock = _fileValidationBlock;
            this.setConfigValidationBlock(cultureName, this.FileValidationBlock);
        }
       

        protected void setConfigValidationBlock(string cultureName,string configfileValidationBlock)
        {
            if (cultureName == null)
            {
                 cultureName = Thread.CurrentThread.CurrentCulture.Name;
            }

            string fileConfig = kanalitik.constanta.Const.PathBaseDir +
                 "Resources\\" + cultureName + @"\" + configfileValidationBlock + ".config";
            if (!System.IO.File.Exists(fileConfig))
            {
                throw new Exception("File Config Model Validation Block not exist");
            }
            var cfgSrc = new FileConfigurationSource(
                fileConfig
                );
            factory = ConfigurationValidatorFactory.FromConfigurationSource(cfgSrc);
       }

        public void setCulture(string cultureName)
        {
            this.setConfigValidationBlock(cultureName, this.FileValidationBlock);
        }

        protected ValidationResults validateModel(T modelToValidate,string cultureName, string ruleSet)
        {
          

            this.setConfigValidationBlock(cultureName, this.FileValidationBlock);

            Validator<T> valFactory = factory.CreateValidator<T>(ruleSet);
            return valFactory.Validate(modelToValidate);
        }

        protected ValidationResults validateModel(T modelToValidate,string ruleSet)
        {
            Validator<T> valFactory = factory.CreateValidator<T>(ruleSet);
            return valFactory.Validate(modelToValidate);
        }

        public void throwErrorSystem(ValidationResults validations)
        {
            throw new Exception("{\"validator\":[ \"" +
                    String.Join("\",\"", validations.Select(a => a.Message).ToArray()) + "\"]}"
                    );
        }
    }
}
