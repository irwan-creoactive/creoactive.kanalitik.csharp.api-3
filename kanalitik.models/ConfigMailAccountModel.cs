﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class ConfigMailAccountModel : BaseModel<ConfigMailAccount>, IBaseModel<ConfigMailAccount>
    {

        public ConfigMailAccountModel() : base("ConfigMailAccountModel") { }

        public async Task<ConfigMailAccount> createOne(ConfigMailAccount data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ConfigMailAccountInsertRuleSet");
            Task<ConfigMailAccount> result = null;
            if (validation.IsValid)
            {
                await this.db.ConfigMailAccount.InsertOneAsync(data);
                result = Task<ConfigMailAccount>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<ConfigMailAccount> updateOne(ConfigMailAccount data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "ConfigMailAccountUpdateRuleSet");

            Task<ConfigMailAccount> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<ConfigMailAccount>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.ConfigMailAccount.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<ConfigMailAccount>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<ConfigMailAccount> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "ConfigMailAccountInsertRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.ConfigMailAccount.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<ConfigMailAccount> data)
        {
            throw new NotImplementedException();
        }

        public async Task<ConfigMailAccount> getById(string id)
        {
            var builderFilter = Builders<ConfigMailAccount>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<ConfigMailAccount> result = null;
            using (var cursor = await this.db.ConfigMailAccount.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<ConfigMailAccount>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<ConfigMailAccount>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.ConfigMailAccount.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<ConfigMailAccount[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<ConfigMailAccount[]> result = null;
            List<ConfigMailAccount> res = new List<ConfigMailAccount>();
            using (var cursor = await this.db.ConfigMailAccount.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<ConfigMailAccount[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
