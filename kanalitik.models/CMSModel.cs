﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{
    public class CMSModel : BaseModel<Cms>, IBaseModel<Cms>
    {
        public CMSModel() : base("CMSModel") { }

        public async Task<Cms> createOne(Cms data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CmsCreateRuleSet");
            Task<Cms> result = null;
            if (validation.IsValid)
            {
                await this.db.Cms.InsertOneAsync(data);
                result = Task<Cms>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<Cms> updateOne(Cms data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CmsUpdateRuleSet");

            Task<Cms> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<Cms>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.Cms.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<Cms>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<Cms> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CmsCreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.Cms.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<Cms> data)
        {
            throw new NotImplementedException();
        }

        public async Task<Cms> getById(string id)
        {
            var builderFilter = Builders<Cms>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<Cms> result = null;
            using (var cursor = await this.db.Cms.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<Cms>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<Cms>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.Cms.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<Cms[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<Cms[]> result = null;
            List<Cms> res = new List<Cms>();
            using (var cursor = await this.db.Cms.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<Cms[]>.Run(() =>
                {
                    return res.ToArray();
                });

            return await result;
        }
    }
}
