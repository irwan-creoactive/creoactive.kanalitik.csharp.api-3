﻿using kanalitik.data.access.documents;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.models
{


    public class StreamMenuModel : BaseModel<StreamMenu>, IBaseModel<StreamMenu>
    {

        public StreamMenuModel() : base("StreamMenuModel") { }

        public async Task<StreamMenu> createOne(StreamMenu data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "CreateRuleSet");
            Task<StreamMenu> result = null;
            if (validation.IsValid)
            {
                await this.db.StreamMenu.InsertOneAsync(data);
                result = Task<StreamMenu>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task<StreamMenu> updateOne(StreamMenu data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var validation = this.validateModel(data, "UpdateRuleSet");

            Task<StreamMenu> result = null;
            if (validation.IsValid)
            {
                var updateBuilder = new UpdateBuilderModel<StreamMenu>().fieldSet(data);
                var whereclause = new BsonDocument(new BsonElement("_id", data.id)).ToJson();
                await this.db.StreamMenu.FindOneAndUpdateAsync(whereclause, updateBuilder);
                result = Task<StreamMenu>.Run(() =>
                {
                    return data;
                });
            }
            else
            {
                this.throwErrorSystem(validation);
            }
            return await result;
        }

        public async Task createMany(List<StreamMenu> data)
        {
            // Jangan Lupa pastikan RuleSet ada!!
            var success = 0;
            foreach (var item in data)
            {
                var validation = this.validateModel(item, "CreateRuleSet");
                if (!validation.IsValid)
                {
                    this.throwErrorSystem(validation);
                    break;
                }
                success = 1;
            }
            if (success == 1)
            {
                await this.db.StreamMenu.InsertManyAsync(data);
            }

        }

        //TODO
        public Task updateMany(List<StreamMenu> data)
        {
            throw new NotImplementedException();
        }

        public async Task<StreamMenu> getById(string id)
        {
            var builderFilter = Builders<StreamMenu>.Filter;
            var filter = builderFilter.Eq(a => a.id, new ObjectId(id));

            Task<StreamMenu> result = null;
            using (var cursor = await this.db.StreamMenu.Find(filter).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        if (doc != null)
                        {
                            result = Task<StreamMenu>.Run(() =>
                            {
                                return doc;
                            });
                        }
                    }
                }
            }
            return await result;
        }

        public async Task<string> deleteById(string id)
        {
            var builderFilter = Builders<StreamMenu>.Filter.Eq(a => a.id, new ObjectId(id));
            await this.db.StreamMenu.FindOneAndDeleteAsync(builderFilter);
            Task<string> result = Task<string>.Run(() =>
            {
                return id;
            });
            return await result;
        }

        public async Task<StreamMenu[]> list(BsonDocument filter, BsonDocument sort, int skip, int limit)
        {

            Task<StreamMenu[]> result = null;
            List<StreamMenu> res = new List<StreamMenu>();
            using (var cursor = await this.db.StreamMenu.Find(filter).Sort(sort).Limit(limit).Skip(skip).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        res.Add(doc);
                    }
                }
            }

            result = Task<StreamMenu[]>.Run(() =>
            {
                return res.ToArray();
            });


            return await result;
        }

    }
}
