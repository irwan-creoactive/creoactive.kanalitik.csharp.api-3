﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;


namespace kanalitik.data.access.validations
{
    public class DateTimeCustomNotNullValidatorData : ValueValidatorData
    {
          /// <summary>
        /// Initializes a new instance of the <see cref="ValueValidatorData"/> class.
        /// </summary>
        public DateTimeCustomNotNullValidatorData()
        { 
        
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueValidatorData"/> class.
        /// </summary>
        /// <param name="name">The name for the instance.</param>
        public DateTimeCustomNotNullValidatorData(string name)
            : base(name, typeof(DateTimeCustomNotNullValidator))
        {
        
        }

        /// <summary>
        /// Creates the <see cref="PersonIDValidator"/> described by the configuration object.
        /// </summary>
        /// <param name="targetType">The type of object that will be validated by the validator.</param>
        /// <returns>The created <see cref="PersonIDValidator"/>.</returns>	
        protected override Microsoft.Practices.EnterpriseLibrary.Validation.Validator DoCreateValidator(Type targetType)
        {
            return new DateTimeCustomNotNullValidator();
        }

      
    }

    [ConfigurationElementType(typeof(DateTimeCustomNotNullValidatorData))]
    public class DateTimeCustomNotNullValidator : Validator<DateTime>
    {
         private DateTime joinedDt;
         public DateTimeCustomNotNullValidator()
            : base(null, null)
        {
            //joinedDt = _joinedDt;
        }

    

        protected override string DefaultMessageTemplate
        {
            get { 
                return "Relieved Date {0}is not valid for Joined Date {1}"; 
            }
        }

        protected override void DoValidate(DateTime objectToValidate, object currentTarget, string key, ValidationResults validationResults)
        {
            if (objectToValidate.Year==1) {
            LogValidationResult(validationResults, this.MessageTemplate, currentTarget, key);
            }
        }
    }

      [AttributeUsage(AttributeTargets.Property
        | AttributeTargets.Field
        | AttributeTargets.Method
        | AttributeTargets.Parameter,
        AllowMultiple = true,
        Inherited = false)]
    public class DateTimeCustomNotNullValidatorAttribute : ValidatorAttribute
    {
         private DateTime joinedDt;

         public DateTimeCustomNotNullValidatorAttribute()
        {
            //this.joinedDt = joinedDt;
        }

        protected override Validator DoCreateValidator(Type targetType)
        {
            return new DateTimeCustomNotNullValidator();

        }
    }

   
}
