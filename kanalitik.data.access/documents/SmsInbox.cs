﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace kanalitik.data.access.documents
{
    public class SmsInbox : Base
    {
        /**
        * description : collection ini menampung seluruh sms yang masuk pada service sms kanalitik; collection ini digunakan sebagai filter sistem untuk memeriksa proses pembayaran. Proses pembayaran difilter melallui sms banking.
        */

        /// <summary>
        /// object id dari collection sms_inbox
        /// </summary>
        public ObjectId id { set; get; }
        /// <summary>
        /// pengirim sms
        /// </summary>
        public string from { set; get; }
        /// <summary>
        /// isi sms
        /// </summary>
        public string message { set; get; }
        /// <summary>
        /// tanggal penerimaan sms
        /// </summary>
        public string date { set; get; }
    }
}
