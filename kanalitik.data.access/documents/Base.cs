﻿using kanalitik.data.access.validations;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    
    public class Base
    {

        public string created_by { set; get; }

        public string modified_by { set; get; }

        [DateTimeCustomNotNullValidator()]
        public DateTime created_date { set; get; }
        [DateTimeCustomNotNullValidator()]
        public DateTime modified_date { set; get; }

    }
}
