﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class RoleUser : Base
    {
        /**
        * description : collection untuk menampung role user.
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection role_user
        /// </summary>

        public string role_id { set; get; }
        /// <summary>
        /// code role user
        /// </summary>

        public string name { set; get; }
        /// <summary>
        /// nama role user
        /// </summary>

        public string description { set; get; }
        /// <summary>
        /// penjelasan singkat dari role user
        /// </summary>

        public List<Privillage> privillages { set; get; }
        /// <summary>
        /// berisi array daftar module yang dapat dikelola oleh role user ini.
        /// </summary>
    }

    public class Privillage
    {
        public string type_object { set; get; }
        /// <summary>
        /// berisi module, account atau stream
        /// </summary>

        public string code_object { set; get; }
        /// <summary>
        /// jika type_object berisi module maka atribut ini berisi id module yang diambil dari collection module; 
        /// jika type_object berisi feature maka atribut ini berisi id module yang diambil dari collection feature; 
        /// jika type_object berisi account maka atribut ini berisi id account yang diambil dari collection account_social_media; 
        /// jika type_object berisi stream maka atribut ini berisi id stream yang diambil dari collection stream_menu;
        /// </summary>

        public string can_change { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true - jika user diberikan akses untuk merubah atau menambah data untuk module, like post comment untuk stream; 
        /// false - jika user tidak diberikan akses tersebut;
        /// </summary>

        public string can_view { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true - jika user diberikan akses untuk view grid dan view detail untuk module, show detail comment untuk stream; 
        /// false - jika user tidak diberikan akses tersebut;
        /// </summary>

        public string can_remove { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true - jika user diberikan akses untuk menghapus data pada module, delete action pada stream, delete schedule post pada account; 
        /// false - jika user tidak diberikan akses tersebut
        /// </summary>

        public string can_post { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true - jika user diberikan akses untuk melakukan posting pada account; 
        /// false - jika user tidak diberikan akses tersebut;
        /// </summary>

        public string can_approve { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true - jika user diberikan akses untuk melakukan approval pada schedule post; 
        /// false - jika user tidak diberikan akses tersebut.
        /// </summary>

    }
}
