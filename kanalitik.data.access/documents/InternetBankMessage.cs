﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace kanalitik.data.access.documents
{
    /// <summary>
    /// collection yang menampung bank message; baik melalui sms atau pun melalui email; efek dari penerimaan pesan ini, user yang melakukan pembayaran akan diberitahu bahwa sudah membayar dan akan aktif paketnya;			
    /// </summary>
    public class InternetBankMessage : Base
    {
        /// <summary>
        /// object id collection internet_bank_message
        /// </summary>
        [BsonId]
        public ObjectId id { set; get; } // object id collection internet_bank_message
        /// <summary>
        /// pengirim
        /// </summary>
        public string from { set; get; } // pengirim 
        /// <summary>
        /// isi pesan
        /// </summary>
        public string body { set; get; } // isi pesan
        /// <summary>
        /// berisi open - close; open jika sms / email masih belum dieksekusi; close jika sms / email sudah dieksekusi;
        /// </summary>
        public string status { set; get; } // berisi open - close; open jika sms / email masih belum dieksekusi; close jika sms / email sudah dieksekusi;
        /// <summary>
        /// diambil dari object id collection sms_inbox;
        /// </summary>
        public string sms_ref_id { set; get; } // diambil dari object id collection sms_inbox;
        /// <summary>
        /// diambil dari object id collection log_pop3_email
        /// </summary>
        public string pop3_ref_id { set; get; } // diambil dari object id collection log_pop3_email
    }
}
