﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class UserLogActivity : Base
    {
        /**
        * description : collection ini menampung seluruh aktifitas yang dilakukan oleh user terhadap sistem kanalitik. log aktifitas ini nanti akan dijadikan bahan report.
        */

        public ObjectId id { set; get; } 
        /// <summary>
        /// object id collection user_log_activity
        /// </summary>

        public string social_media_account_id { set; get; } 
        /// <summary>
        /// akun sosial media yang digunakan; 
        /// diambil dari object id collection account_social_media
        /// </summary>

        public string stream_type { set; get; } 
        /// <summary>
        /// kode stream tempat dilakukannya aksi;
        /// </summary>

        public string content { set; get; } 
        /// <summary>
        /// isian aksi; 
        /// jika aksinya berupa posting, maka isian dari posting akan disimpan di sini;
        /// </summary>

        public string file_id { set; get; } 
        /// <summary>
        /// file attachment
        /// </summary>

        public string post_id { set; get; } 
        /// <summary>
        /// id post setelah aksi; 
        /// atribut ini diisi jika ternyata user melakukan post
        /// </summary>

        public string client_id { set; get; } 
        /// <summary>
        /// id client user; 
        /// diambil dari object id collection client;
        /// </summary>

        public string user_id { set; get; } 
        /// <summary>
        /// id user yang melakukan aksi
        /// </summary>

        public string message { set; get; } 
        /// <summary>
        /// pesan sistem; 
        /// jika aksi gagal;
        /// </summary>

        public string action_name { set; get; } 
        /// <summary>
        /// nama yang disematkan untuk aksi yang dilakukan oleh user;
        /// </summary>
        
    }
}
