﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class GeneralConfiguration : Base
    {
        /**
        * description : konfigurasi umum terkait settingan pada kanalitik tersimpan pada collection ini; contoh due date email activation, reset password due date dan sebagainya;
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id dari collection general_configuration
        /// </summary>

        public string name_config { set; get; }
        /// <summary>
        /// nama unik dari konfigurasi; 
        /// nama unik ini yang akan digunakan oleh sistem kanalitik sebagai penanda; 
        /// </summary>

        public string value_config { set; get; }
        /// <summary>
        /// isian value sesuai dengan nama config; 
        /// misal nama config diisi email_activation_due_date dan value_config berisi 7, 
        /// maka batas aktifasi user selama 7 hari;
        /// </summary>

    }
}
