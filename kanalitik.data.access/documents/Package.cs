﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace kanalitik.data.access.documents
{
    /// <summary>
    /// merupakan collection packaage. Berisi data lengkap mengenai package yang tersedia.			
    /// </summary>
    public class Package : Base
    {
        [BsonId]
        /// <summary>
        /// object id dari collection package
        /// </summary>
        public ObjectId id { set; get; }
        /// <summary>
        /// nama package
        /// </summary>
        public double price { set; get; }
        /// <summary>
        /// berisi array penjelasan tentang social media
        /// </summary>
        public string[] social_media { set; get; }
        /// <summary>
        /// berisi array penjelasan tentang blog
        /// </summary>
        public string[] blog { set; get; }
        /// <summary>
        /// berisi array penjelasan tentang news portal
        /// </summary>
        public string[] news_portal { set; get; }
        /// <summary>
        /// berisi true - false; fitur ini digunakan salah satunya untuk package demo. Mengingat bahwa demo adalah package tersendiri dengan keterbatasan fitur, maka package demo tetap dibuat namun tidak ditampilkan.
        /// </summary>
        public bool is_show { set; get; }
        /// <summary>
        /// berisi daftar module atau menu yang melekat pada package yang dipilih
        /// </summary>
        public List<ModulePackage> module { set; get; }
    }

    public class ModulePackage
    {
        /// <summary>
        /// object id dari collection module
        /// </summary>
        string module_id { set; get; }
        /// <summary>
        /// berisi true - false; true - jika package ini mengakomodir untuk change module; false - jika package ini tidak mengakomodir untuk change module; change adalah create dan edit.
        /// </summary>
        bool can_change { set; get; }
        /// <summary>
        /// berisi true - false; true - jika package ini mengakomodir untuk view module; false - jika package ini tidak mengakomodir untuk veiw module; View adalah show grid data dan show detail data.
        /// </summary>
        bool can_view { set; get; }
        /// <summary>
        /// berisi true - false; true - jika package ini mengakomodir untuk remove module; false - jika package ini tidak mengakomodir untuk remove module; remove adalah segala aksi yang terkait delete data.
        /// </summary>
        bool can_remove { set; get; }
    }
}
