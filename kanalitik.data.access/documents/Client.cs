﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace kanalitik.data.access.documents
{
    public class Client : Base
    {
        /// <summary>
        /// merupakan collection client. Bisa dibilang juga collection project. Misal Indosat punya project dengan syndicate, maka isian dari client ini bisa diberinama Indosat. Walau pun isi di dalamnya bisa beda-beda, ada IM3, indosat mania, dan seterusnya.			
        /// </summary>
        [BsonId]
        public ObjectId id { set; get; }
        /// <summary>
        /// nama client
        /// </summary>
        public string name { set; get; }
        /// <summary>
        /// Penjelasan package yang diambil oleh client ini. Package ini hanya boleh berisi satu package. tidak untuk lebih dari satu package. Jika sudah upgrade package, maka package yang tampil di sini adalah package yang paling mahal.
        /// </summary>
        public List<PackageClient> packages { set; get; }
        /// <summary>
        /// user-id pemilik client : diambil dari collection user._id
        /// </summary>
        public string owned_user { set; get; }
        /// <summary>
        /// daftar member yang berhak mengelola client/project ini. Berisi list user - bisa lebih dari satu user.
        /// </summary>
        public List<MemberClient> members { set; get; }
        /// <summary>
        /// berisi active dan nonactive (true-false). client ini dapat akses atau tidak, status ini dipengaruhi oleh pembayaran package client.
        /// </summary>
        public bool status { set; get; }
    }

    public class PackageClient
    {
        /// <summary>
        /// object id dari package yang diambil
        /// </summary>
        public string id { set; get; }
        /// <summary>
        /// nama package yang diambil
        /// </summary>
        public string name { set; get; }
        /// <summary>
        /// tanggal package mulai aktif
        /// </summary>
        public long active_since { set; get; }
        /// <summary>
        /// tanggal package berakhir
        /// </summary>
        public long expired_at { set; get; }
    }


    public class MemberClient
    {
        /// <summary>
        /// user-id member : diambil dari collection user._id
        /// </summary>
        public string id { set; get; }
        /// <summary>
        /// role_client_id yang melekat pada member : diambil dari collection role_client._id
        /// </summary>
        public string role_user { set; get; }
        /// <summary>
        /// berisi active dan nonactive (true-false). user boleh akses atau tidak, bergantung pada status ini.
        /// </summary>
        public bool status { set; get; }
    }

}
