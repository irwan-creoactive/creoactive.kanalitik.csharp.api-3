﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace kanalitik.data.access.documents
{
    public class RequestDemo : Base
    {
        /**
        * description : collection ini menampung daftar user yang meminta demo;
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id dari collection request demo;
        /// </summary>

        public string user_id { set; get; }
        /// <summary>
        /// user id yang meminta request demo; 
        /// diambil dari collection user; 
        /// </summary>

        public string package_id { set; get; }
        /// <summary>
        /// package yang dinginkan untuk demo; 
        /// diambil dari collection package; 
        /// </summary>

        public string date_request { set; get; }
        /// <summary>
        /// tanggal permintaan
        /// </summary>

        public string approved { set; get; }
        /// <summary>
        /// berisi true - false; 
        /// true jika request demo disetujui; 
        /// false jika request demo ditolak
        /// </summary>

        public string date_approved { set; get; }
        /// <summary>
        /// tanggal approved jika disetujui
        /// </summary>

        public string date_expired { set; get; }
        /// <summary>
        /// tanggal expired;
        /// </summary>

    }
}
