﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace kanalitik.data.access.documents
{
    /// <summary>
    /// collection ini menampung pemetaaan stream terhadap menu yang dibuat oleh user;			
    /// </summary>
    public class StreamMenu : Base
    {
        /// <summary>
        /// object id collection stream_menu
        /// </summary>
        [BsonId]
        public ObjectId id { set; get; }

        /// <summary>
        /// client id pemilik menu; diambil dari object id collection client
        /// </summary>
        public string client_id { set; get; }

        /// <summary>
        /// client id pemilik menu; diambil dari object id collection client
        /// </summary>
        public string title_menu { set; get; }

        /// <summary>
        /// nama menu
        /// </summary>
        public string icon_cls { set; get; }

        /// <summary>
        /// berisi daftar media yang didaftarkan untuk stream menu ini
        /// </summary>
        public List<Media> medias { set; get; } // berisi daftar stream yang melekat pada menu ini;
    }

    public class Media : Base
    {
        /// <summary>
        /// berisi account_social_media atau keyword
        /// </summary>
        public string media_type { set; get; }

        /// <summary>
        /// diambil dari account_social_media_id jika account_type == account_social media; 
        /// jika account_type = keyword, diambil dari keyword_id
        /// </summary>
        public string media_id { set; get; }

        /// <summary>
        /// daftar asset/stream dari account_social_media
        /// </summary>
        public List<Asset> assets { set; get; }

    }

    public class Asset : Base
    {
        /// <summary>
        /// judul stream
        /// </summary>
        public string stream_title { set; get; }

        /// <summary>
        /// tipe stream
        /// </summary>
        public string stream_type { set; get; }

        /// <summary>
        /// isian keyword; atribut ini akan diisi jika stream merupakan stream keyword
        /// </summary>
        public string keyword_by_user { set; get; }

        /// <summary>
        /// isian rss; atribut ini akan diisi jika stream diambil dari rss;
        /// </summary>
        public string rss_url_by_user { set; get; }

        /// <summary>
        /// alamat url rss
        /// </summary>
        public string web_url_by_user { set; get; }

        /// <summary>
        /// durasi refresh;
        /// </summary>
        public int refresh_time_in_second { set; get; }

        /// <summary>
        /// urutan stream
        /// </summary>
        public int sequence { set; get; }

        /// <summary>
        /// berisi true - false; 
        /// true jika stream ini diclose; 
        /// false jika stream ini tidak diclose;
        /// </summary>
        public bool is_close { set; get; }

    }
}
