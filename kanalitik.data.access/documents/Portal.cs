﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class Portal : Base
    {
        /**
        * description : collection ini menampung daftar portal yang dimasukkan oleh user;
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// obejct id dari collection portal
        /// </summary>

        public string client_id { set; get; }
        /// <summary>
        /// berisi object id collection client
        /// </summary>

        public string portal { set; get; }
        /// <summary>
        /// nama portal
        /// </summary>

        public string url_rss { set; get; }
        /// <summary>
        /// alamat rss dari portal
        /// </summary>

    }
}
