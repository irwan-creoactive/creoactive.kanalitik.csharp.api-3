﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kanalitik.data.access.documents
{
    public class DiscussionBoard : Base
    {
        /**
        * description : collection ini menampung diskusi yang terjadi antara client member; 
        */

        public ObjectId id { set; get; }
        /// <summary>
        /// object id collection discussion_board
        /// </summary>

        public string post_id { set; get; }
        /// <summary>
        /// post_id yang diambil dari api social media; 
        /// identifikasi post yang digunakan sebagai rujukan diskusi; 
        /// </summary>

        public string account_id { set; get; }
        /// <summary>
        /// berisi id dari account social media yang postingannya dijadikan bahan diskusi; 
        /// account_id ini diambil dari api sistem social media;
        /// </summary>

        public string username { set; get; }
        /// <summary>
        /// nama user yang membuat post (bahan diskusi); 
        /// diambil dari api sistem social media;
        /// </summary>

        public string url_avatar { set; get; }
        /// <summary>
        /// profile picture account social media yang dimasukkan sebagai bahan diskusi;
        /// </summary>

        public string post_from_screen_name { set; get; }
        /// <summary>
        /// nama yang ditampilkan di layar dari user/akun/channel yang melakukan posting; 
        /// diambil dari api social media
        /// </summary>

        public string post_name { set; get; }
        /// <summary>
        /// name/judul link (di dalam kotak); 
        /// diambil dari api social media
        /// </summary>

        public string post_caption { set; get; }
        /// <summary>
        /// caption link; 
        /// diambil dari api social media
        /// </summary>

        public string post_description { set; get; }
        /// <summary>
        /// description link; 
        /// diambil dari api social media
        /// </summary>

        public string post_from_url { set; get; }
        /// <summary>
        /// url dari user/akun/channel yang melakukan posting - yang postingannya dijadikan bahan diskusi;
        /// </summary>

        public string post_url { set; get; }
        /// <summary>
        /// url dari postingan yang dijadikan bahan diskusi;
        /// </summary>

        public string post_provider { set; get; }
        /// <summary>
        /// sumber post 
        /// (digunakan oleh google+, provider ini menunjukkan asal dari mana post ini didapatkan - 
        ///  sepertinya kelak seluruh API google bakal dibuatkan satu pintu dan ini adalah pembedanya)
        /// </summary>

        public string post_type { set; get; }
        /// <summary>
        /// type post (status ; photo ; video)
        /// </summary>

        public string post_text { set; get; }
        /// <summary>
        /// message atau comment atau text postingan yang dijadikan bahan diskusi;
        /// </summary>

        public string post_time { set; get; }
        /// <summary>
        /// tanggal dan waktu postingan terjadi
        /// </summary>

        public string post_entities_media { set; get; }
        /// <summary>
        /// attachment media; 
        /// berisi url gambar jika postingan disertai gambar; 
        /// url video jika postingan disertai video dan seterusnya;
        /// </summary>

        public string post_entities_media_type { set; get; }
        /// <summary>
        /// jenis media yang menyertai postingan; 
        /// image jika postingan disertai gambar; 
        /// video jika postingan disertai video; dan seterusnya;
        /// </summary>

        public string social_media_id { set; get; }
        /// <summary>
        /// berisi id dari collection social media; 
        /// mencatatkan postingan yang dijadikan bahan diskusi ini didapat dari akun sosial media yang mana.
        /// </summary>

        public string client_id { set; get; }
        /// <summary>
        /// berisi id dari collection client; 
        /// menandakan board diskusi ini untuk client yang mana.
        /// </summary>

        public string caption_discussion { set; get; }
        /// <summary>
        /// komentar pengantar dari user yang memasukkan postingan sebagai bahan diskusi.
        /// </summary>

        public List<CommentDiscussion> comments { set; get; }
        /// <summary>
        /// berisi array object dari komen / tanggapan dari discussion_board
        /// </summary>

        public string session_user_login { set; get; }
        /// <summary>
        /// user kanalitik yang memsukkan postingan menjadi bahan diskusi;
        /// </summary>

    }

    public class CommentDiscussion
    {
        string sequence { set; get; }
        /// <summary>
        /// urutan komentar
        /// </summary>

        string user_id { set; get; }
        /// <summary>
        /// user id yang memberikan komentar; diambil dari object id dari collection user
        /// </summary>

        string client_id { set; get; }
        /// <summary>
        /// client id di mana user terdaftar
        /// </summary>

        string message { set; get; }
        /// <summary>
        /// pesan / komentar / tanggapan dari user
        /// </summary>

        public string created_by { set; get; }
        /// <summary>
        /// id user pembuat data
        /// </summary>

        public string modified_by { set; get; }
        /// <summary>
        /// id user yang terakhir merubah data
        /// </summary>

        public string created_date { set; get; }
        /// <summary>
        /// tanggal pembuatan data
        /// </summary>

        public string modified_date { set; get; }
        /// <summary>
        /// tanggal perubahan data
        /// </summary>

    }
}
