﻿using Akka.Actor;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.akka.actors.mail
{
    public class RunMailServiceActor : UntypedActor
    {
        IMailService svc;
        public RunMailServiceActor(IMailService _svc)
        {
            svc = _svc;
        }

        protected override void OnReceive(object request)
        {
            var req = (ActorRequest<BaseRequest>)request;
            var res = svc.serviceRun(req);
            Sender.Forward(res);
        }
    }
}
