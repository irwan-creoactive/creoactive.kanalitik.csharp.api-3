﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Configuration;
using Akka;
using Akka.Actor;
using Akka.Configuration.Hocon;
using kanalitik.akka.actors;
using kanalitik.data.contract;
using kanalitik.akka.actors.core;
using kanalitik.constanta;
using kanalitik.data.contract.apiResponse;
using kanalitik.data.contract.apiRequest;
using kanalitik.akka.actors.user;
using System.Web.Http.Cors;
using kanalitik.akka.actors.mail;

namespace kanalitik.api.Controllers
{
    [EnableCors(origins: "syndicate73.ddns.net:44304", headers: "*", methods: "GET, POST, OPTIONS")]
    public class MailController : ApplicationController
    {
        public MailController()
        {

        }

        [HttpPost]
        public async Task<ActorResponse<MailResponse>> MailSave(MailSaveRequestApi request)
        {
            #region //Contoh Direct
            //var greeter = System.ActorSelection(String.Format(Const.actorUserPath, "register"));
            //var result = await greeter.Ask<Object>(new ActorRequest {  Interface = "hai" }, Const.DefaultTimeout);
            //return Ok(new { msg = result });
            #endregion

            var actorIndex = await GetActor<MailSaveActor>();
            return await actorIndex.Ask<ActorResponse<MailResponse>>(request, Const.DefaultTimeout);
        }
    }
}
