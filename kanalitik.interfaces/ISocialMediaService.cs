﻿using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.request.socialmedia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.interfaces
{
    public interface ISocialMediaService
    {


        /// <summary>
        /// load data stream
        /// refresh stream
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<StreamComponentListResponse> streamList(ActorRequest<AccountStreamRequest> form);

        /// <summary>
        /// action stream
        /// like unlike, dislike
        /// comment, delete
        /// favorite unfavorite
        /// tweet, quote, delete
        /// dll
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        ActorResponse<StreamComponentResponse> streamAction(ActorRequest<ActionStreamRequest> form);

        /// <summary>
        /// add collection account_social_media
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string account_social_media._id</returns>
        ActorResponse<AccountResponse> accountSave(ActorRequest<AccountRequest> request);

        /// <summary>
        /// delete collection account_social_media
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string account_social_media._id</returns>
        ActorResponse<AccountResponse> accountDelete(ActorRequest<AccountRequest> request);

        /// <summary>
        /// Mengembalikan daftar account social media yang ada pada collection account_social_media
        /// </summary>
        /// <param name="request">
        ///     Berisi 
        ///         Media : berisi facebook; twitter; dst
        ///             Jika berisi all, maka yang dikembalikan adalah seluruh daftar account.
        ///             Jika berisi facebook; maka yang dikembalikan adalah account facebook saja.
        ///         
        ///         IsNonAuthorize : berisi true - false
        ///             Jika berisi true, maka yang dikembalikan adalah daftar owned account.
        ///             Jika berisi false, maka yang dikembalikan adlaah daftar other account.
        /// </param>
        /// <returns>List account yang diambil dari collection account_social_media</returns>
        ActorResponse<AccountListResponse> accountList(ActorRequest<AccountFilterRequest> request);

        /// <summary>
        /// Menampilkan daftar account child seperti facebook page dan facebook group
        /// </summary>
        /// <param name="request"></param>
        /// <returns>List account yang diambil dari collection account_social_media</returns>
        ActorResponse<AccountListResponse> accountChildList(ActorRequest<AccountFilterRequest> request);

        /// <summary>
        /// Menambahkan account_social_media yang dipilih pada stream_menu.
        /// 
        /// proses :
        ///     add stream_menu.medias.media
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string account_social_media._id</returns>
        ActorResponse<AccountStreamResponse> accountStreamSave(ActorRequest<AccountRequest> request);

        /// <summary>
        /// Menghapus account_social_media yang dipilih pada stream_menu.
        /// 
        /// proses :
        ///     delete stream_menu.medias.media_id sesuai dengan request.AccSocialMediaId
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string account_social_media._id</returns>
        ActorResponse<AccountStreamResponse> accountStreamDelete(ActorRequest<AccountRequest> request);

        /// <summary>
        /// Menambahkan asset pada stream menu
        /// 
        /// proses :
        ///     add stream_menu.medias.assets
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string account_social_media._id</returns>
        ActorResponse<StreamResponse> accountStreamAssetSave(ActorRequest<StreamRequest> request);

        /// <summary>
        /// Menghapus asset pada stream menu
        /// 
        /// proses :
        ///     delete stream_menu.medias.assets
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string stream._id</returns>
        ActorResponse<StreamResponse> accountStreamAssetDelete(ActorRequest<StreamRequest> request);

        /// <summary>
        /// Menambahkan data keyword
        /// 
        /// proses :
        ///     add collection keyword
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<KeywordResponse> keywordSave(ActorRequest<KeywordRequest> request);

        /// <summary>
        /// Merubah data keyword
        /// 
        /// proses :
        ///     update keyword.keyword = request.Keyword
        ///     update keyword.social_media = request.SocialMedia
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<KeywordResponse> keywordEdit(ActorRequest<KeywordRequest> request);

        /// <summary>
        /// Menghapus data keyword
        /// 
        /// proses :
        ///     delete keyword sesuai dengan request.KeywordId
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<KeywordResponse> keywordDelete(ActorRequest<KeywordRequest> request);

        /// <summary>
        /// Retrieve seluruh data pada collection keyword sesuai dengan client id
        /// 
        /// proses :
        ///     menampilkan data pada keyword dengan filter client-id
        /// </summary>
        /// <param name="request"></param>
        /// <returns>List object Keyword</returns>
        ActorResponse<KeywordListResponse> keywordList(ActorRequest<ClientSettingRequest> request);

        /// <summary>
        /// Menambahkan keyword yang dipilih pada stream_menu.
        /// 
        /// proses :
        ///     add stream_menu.medias.media_type = "keyword"
        ///     add stream_menu.medias.media_id = request.KeywordId
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<KeywordStreamResponse> keywordStreamSave(ActorRequest<KeywordStreamRequest> request);

        /// <summary>
        /// Menghapus keyword yang dipilih pada stream_menu.
        /// 
        /// proses :
        ///     delete stream_menu.medias
        ///         where
        ///             stream_menu.medias.medias.account_type = "keyword"
        ///             stream_menu.medias.medias.account_id = request.KeywordId
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<KeywordStreamResponse> keywordStreamDelete(ActorRequest<KeywordStreamRequest> request);

        /// <summary>
        /// Menambahkan portal yang dipilih pada stream_menu.
        /// 
        /// proses :
        ///     add stream_menu.medias.media_type = "portal"
        ///     add stream_menu.medias.media_id = request.PortalId
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<PortalStreamResponse> portalStreamSave(ActorRequest<PortalStreamRequest> request);

        /// <summary>
        /// Menghapus portal yang dipilih pada stream_menu.
        /// 
        /// proses :
        ///     delete stream_menu.medias
        ///         where
        ///             add stream_menu.medias.media_type = "portal"
        ///             add stream_menu.medias.media_id = request.PortalId
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string keyword._id</returns>
        ActorResponse<PortalStreamResponse> portalStreamDelete(ActorRequest<PortalStreamRequest> request);

        /// <summary>
        /// Menambahkan asset pada stream menu
        /// 
        /// proses :
        ///     add stream_menu.medias.assets
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string account_social_media._id</returns>
        ActorResponse<StreamResponse> keywordStreamAssetSave(ActorRequest<StreamRequest> request);

        /// <summary>
        /// Menghapus asset pada stream menu
        /// 
        /// proses :
        ///     delete stream_menu.medias.assets
        /// </summary>
        /// <param name="request"></param>
        /// <returns>string stream._id</returns>
        ActorResponse<StreamResponse> keywordStreamAssetDelete(ActorRequest<StreamRequest> request);
    }
}
