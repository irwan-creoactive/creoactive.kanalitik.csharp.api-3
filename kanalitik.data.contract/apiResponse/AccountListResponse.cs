﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class AccountListResponse : BaseResponse
    {
        public List<Account> Accounts { set; get; }
    }

    public class Account
    {
        public string AccSocialMediaId { set; get; }
        public string ClientId { set; get; }
        public string SocialMedia { set; get; }
        public string Media { set; get; }
        public string AccountName { set; get; }
        public string Email { set; get; }
        public string ProfilePicture { set; get; }
        public string FBPageCategory { set; get; }
        public string FBPagePermission { set; get; }
        public string ParentSocialMediaId { set; get; }
        public string IsHaveYoutubeAccount { set; get; }
    }
}
