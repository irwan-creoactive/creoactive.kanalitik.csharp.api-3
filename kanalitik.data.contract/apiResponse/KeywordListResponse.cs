﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class KeywordListResponse : BaseResponse
    {
        public List<KeywordResponse> KeywordList { set; get; }
    }
}
