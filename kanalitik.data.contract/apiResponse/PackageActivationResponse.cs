﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class PackageActivationResponse : BaseResponse
    {
        public string package { set; get; }
    }
}
