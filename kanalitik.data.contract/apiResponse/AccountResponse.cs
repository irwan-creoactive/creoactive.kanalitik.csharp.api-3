﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class AccountResponse : BaseResponse
    {
        public string AccountSocialMediaId { set; get; }
    }
}
