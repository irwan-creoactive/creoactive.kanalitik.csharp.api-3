﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class AccountSettingResponse : BaseResponse
    {
        public string UserId { set; get; }
    }
}
