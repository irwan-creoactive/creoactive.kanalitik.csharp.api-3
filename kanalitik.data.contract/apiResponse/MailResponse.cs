﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class MailResponse : BaseResponse
    {
        public string email { set; get; }
    }
}
