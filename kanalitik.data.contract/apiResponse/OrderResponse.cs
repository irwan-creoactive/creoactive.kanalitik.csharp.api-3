﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class OrderResponse : BaseResponse
    {
        public string email { set; get; }
        public string package { set; get; }
    }
}
