﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiResponse
{
    public class KeywordResponse : BaseResponse
    {
        public string KeywordId { set; get; }
        public string ClientId { set; get; }
        public List<string> SocialMedia { set; get; }
    }
}
