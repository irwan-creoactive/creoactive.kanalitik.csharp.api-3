﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class ClientSettingRequestApi : BaseRequest
    {
        public string ClientId { set; get; }
        public string ClientName { set; get; }
        public string UserId { set; get; }
        public string BusinessType { set; get; }
    }
}
