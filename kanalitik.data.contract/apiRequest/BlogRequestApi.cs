﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class BlogRequestApi : BaseRequest
    {
        public string _id { set; get; }
        public string Title { set; get; }
        public string Content { set; get; }
        public string Image { set; get; }
        public bool IsPublish { set; get; }
    }
}
