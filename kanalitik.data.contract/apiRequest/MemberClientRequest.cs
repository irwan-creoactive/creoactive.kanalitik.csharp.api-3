﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class MemberClientRequest : BaseRequest
    {
        public string UserId { set; get; }
        public string RoleUser { set; get; }
        public string ClientId { set; get; }
    }
}
