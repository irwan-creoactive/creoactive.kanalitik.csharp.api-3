﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class KeywordStreamRequestApi : BaseRequest
    {
        public string StreamId { set; get; }
        public string Media { set; get; }
        public string Group { set; get; }
        public string StreamName { set; get; }
        public string UrlWeb { set; get; }
        public string UrlRSS { set; get; }
        public string Categories { set; get; }
        public string Contents { set; get; }

        public string KeywordId { set; get; }
        public string ClientId { set; get; }
        public string Keyword { set; get; }
        public List<string> SocialMedia { set; get; }
    }
}
