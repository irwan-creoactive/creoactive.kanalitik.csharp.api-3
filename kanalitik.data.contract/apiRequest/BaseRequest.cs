﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class BaseRequest
    {
        public string UserId { set; get; }
        public string CultureName { set; get; }
        public string TokenUser { set; get; }
    }
}
