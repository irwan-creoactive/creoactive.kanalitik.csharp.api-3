﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class KeywordRequestApi : BaseRequest
    {
        public string KeywordId { set; get; }
        public string ClientId { set; get; }
        public string Keyword { set; get; }
        public List<string> SocialMedia { set; get; }
    }
}
