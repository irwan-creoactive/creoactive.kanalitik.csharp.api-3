﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class InviteClientRequest : BaseRequest
    {
        public string Name { set; get; }
        public string Email { set; get; }
        public string ClientId { set; get; }
        public string RoleUser { set; get; }
    }
}
