﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.data.contract.apiRequest
{
    public class AccountStreamRequestApi : BaseRequest
    {
        public string StreamId { set; get; }
        public string Media { set; get; }
        public string Group { set; get; }
        public string StreamName { set; get; }
        public string UrlWeb { set; get; }
        public string UrlRSS { set; get; }
        public string Categories { set; get; }
        public string Contents { set; get; }

        public string AccSocialMediaId { set; get; }
        public string ClientId { set; get; }
        public string SocialMedia { set; get; }
        //public string Media { set; get; }
        public string AccountName { set; get; }
        public string Email { set; get; }
        public string ProfilePicture { set; get; }
        public string FBPageCategory { set; get; }
        public string FBPagePermission { set; get; }
        public string ParentSocialMediaId { set; get; }
        public string IsHaveYoutubeAccount { set; get; }
    }
}
