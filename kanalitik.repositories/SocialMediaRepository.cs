﻿using kanalitik.appconfig;
using kanalitik.data.contract;
using kanalitik.data.contract.apiRequest;
using kanalitik.data.contract.apiResponse;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public partial class SocialMediaRepository : BaseRepository, ISocialMediaService
    {
        KanalitikAppConfig cfg;
        public SocialMediaRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
        }

        public SocialMediaRepository()
        {

        }

        public ActorResponse<StreamComponentListResponse> streamList(ActorRequest<request.socialmedia.AccountStreamRequest> form)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<StreamComponentResponse> streamAction(ActorRequest<request.socialmedia.ActionStreamRequest> form)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountResponse> accountSave(ActorRequest<request.socialmedia.AccountRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountResponse> accountDelete(ActorRequest<request.socialmedia.AccountRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountListResponse> accountList(ActorRequest<request.socialmedia.AccountFilterRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountListResponse> accountChildList(ActorRequest<request.socialmedia.AccountFilterRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountStreamResponse> accountStreamSave(ActorRequest<request.socialmedia.AccountRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<AccountStreamResponse> accountStreamDelete(ActorRequest<request.socialmedia.AccountRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<StreamResponse> accountStreamAssetSave(ActorRequest<request.socialmedia.StreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<StreamResponse> accountStreamAssetDelete(ActorRequest<request.socialmedia.StreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<KeywordResponse> keywordSave(ActorRequest<request.socialmedia.KeywordRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<KeywordResponse> keywordEdit(ActorRequest<request.socialmedia.KeywordRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<KeywordResponse> keywordDelete(ActorRequest<request.socialmedia.KeywordRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<KeywordListResponse> keywordList(ActorRequest<request.socialmedia.ClientSettingRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<KeywordStreamResponse> keywordStreamSave(ActorRequest<request.socialmedia.KeywordStreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<KeywordStreamResponse> keywordStreamDelete(ActorRequest<request.socialmedia.KeywordStreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<PortalStreamResponse> portalStreamSave(ActorRequest<request.socialmedia.PortalStreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<PortalStreamResponse> portalStreamDelete(ActorRequest<request.socialmedia.PortalStreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<StreamResponse> keywordStreamAssetSave(ActorRequest<request.socialmedia.StreamRequest> request)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<StreamResponse> keywordStreamAssetDelete(ActorRequest<request.socialmedia.StreamRequest> request)
        {
            throw new NotImplementedException();
        }
    }
}
