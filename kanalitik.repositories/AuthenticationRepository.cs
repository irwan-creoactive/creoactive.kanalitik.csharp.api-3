﻿using kanalitik.appconfig;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanalitik.repositories
{
    public partial class AuthenticationRepository:BaseRepository,IAuthenticationService
    {
        KanalitikAppConfig cfg;
        public AuthenticationRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
        }

        public AuthenticationRepository()
        {

        }
    }
}
