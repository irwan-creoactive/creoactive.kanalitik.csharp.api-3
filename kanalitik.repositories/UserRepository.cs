﻿using kanalitik.data.access;
using kanalitik.data.access.documents;
using kanalitik.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Threading;
using kanalitik.data.contract;
using kanalitik.data.contract.apiResponse;
using kanalitik.data.contract.apiRequest;
using kanalitik.appconfig;
using kanalitik.models;
using kanalitik.request.user;

namespace kanalitik.repositories
{
    public partial class UserRepository : BaseRepository, IUserService
    {
        KanalitikAppConfig cfg;

        protected UserModel userModel;

        public UserRepository(KanalitikAppConfig _cfg)
        {
            this.cfg = _cfg;
            this.userModel = new UserModel();
        }

        public UserRepository()
        {

        }

        public ActorResponse<BaseResponse> checkAccountIsExist(ActorRequest<string> request)
        {
            var response = new ActorResponse<BaseResponse>();

            var accountExist = this.userModel.isEmailExist(request.data).GetAwaiter().GetResult();

            bool status = accountExist != null;

            //response.Result = new BaseResponse { message = status?"", messageCode = "", status = status };
            return response;
        }

        public ActorResponse<RegisterResponse> register(ActorRequest<RegisterRequest> request)
        {
            ActorResponse<RegisterResponse> response = new ActorResponse<RegisterResponse>();
            response.Result = new RegisterResponse { };

            try
            {

                var validation = request.validateModel(request.data, request.data.CultureName, "UserRegisterRuleSet");

                #region Validation Block
                if (!validation.IsValid)
                {
                    request.throwErrorSystem(validation);
                }
                #endregion

                #region insert document
                this.userModel.setCulture(request.data.CultureName);

                string unEncryptPass = kanalitik.utility.SecurityUtility.generatePassword();
                string encrptedPass = kanalitik.utility.SecurityUtility.encrypt(unEncryptPass);
                this.userModel.createOne(new User
                {
                    email = request.data.email,
                    name = request.data.name,
                    password = encrptedPass,
                    business_type = request.data.business_type,
                    business_type_name = request.data.business_type_name,
                    contact_number = request.data.contact_number,
                    created_by = request.data.UserId,
                    modified_by = request.data.UserId,
                    created_date = DateTime.UtcNow,
                    modified_date = DateTime.UtcNow,
                    role_user = "1" // Default account Owner Free Member
                }).GetAwaiter().GetResult();
                #endregion

                #region Send Email Activation Link
                // TODO
                #endregion


                #region Attribute Success
                response.Result.message = "OK";
                response.Result.status = true;
                response.Result.success = true;
                #endregion

            }
            catch (Exception e)
            {
                response.Result.message = e.Message;
            }

            return response;
        }

        public ActorResponse<UserProfileResponse> getProfile(ActorRequest<BaseRequest> req)
        {
            throw new NotImplementedException();
        }

        public ActorResponse<BaseResponse> saveProfile(ActorRequest<BaseRequest> req)
        {
            throw new NotImplementedException();
        }


    }
}
